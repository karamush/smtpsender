unit executor;
{ 03.04.017 }
interface

uses
  windows, StrUtils, SysUtils, classes, utils, smtpsender_unit;

type
  TVarReplacer = class;
  TCommandListParser = class;
  TCommandExecutor = class;
  TCmdParser = class;

  {
    �����, ������� ��������������� ��������� �������, ������ �������� ���������� � ���.
  }
  TCommandExecutor = class
  private
    __utils: TUtils;
    varManager: TVarReplacer;
  public
    smtp: TSMTPSender;
    constructor Create(VarReplacer: TVarReplacer = nil);
    destructor Destroy;
    function ExecCommand(ACommand: string): Boolean;
    procedure PrintCommandError(command, error_text: string; terminate: Boolean
      = true);
  published
  end;

  {
    ��������� ����� ��� ������ ����������. ��������� ����� ����, �� ���� �
    ���������� ����������, ������� ������ ��������.
  }
  TVarReplacer = class(TParamParser)
  private
    FOpenChar: string;
    FCloseChar: string;
    procedure SetCloseChar(const Value: string);
    procedure SetOpenChar(const Value: string);
  public
    constructor Create(lines: TStrings = nil);
    procedure AddVar(var_name: string; var_value: string = '');
    function GetVar(var_name: string; default_value: string = ''): string;
    function ReplaceVars(input_string: string): string;
  published
    property OpenChar: string read FOpenChar write SetOpenChar;
    property CloseChar: string read FCloseChar write SetCloseChar;
  end;

  {
    ���� ����� ��������� �� ���� ������ � �������� ��� ���������. ���� ����
    � �����. � ��������� ������ ��� �������, �������� ��, ���� ��� � ���� ������.
    ����� �� ����� ��������� :)
  }
  TCommandListParser = class
  private
    //��� ����� ��������� ��� ������ ������, � ������ ������ ���������
    FCommandList: TStringList;
    //����������� ��� ������������ �������
    FCommandDelimiter: string;
    //�������� ��������� ����������� �� ������ �������� ������
    function replaceDelimiter(from_text: string): string;
    //... ������ ��� FCommandDelimiter
    procedure SetCommandDelimiter(const Value: string);
    function FGetCommandCount: integer;
  public
    VarManager: TVarReplacer;
    //�����������. ����� ����� �������� ������ ������!
    constructor Create(ACommandList: string = ''; varsManager: TVarReplacer =
      nil);
    //���������� ������ ������, ������� ��������� ����������� ��� ������������ ������
    procedure ParseCommandList(ACommandList: string = '');
    //�������� ��������� ������ � �������� (����� ���� ��������� � ������������)
    procedure AddCommand(cmd_string: string);
    //��������� � ����� ���������� ������ ������ �� �����
    function LoadFromFile(AFileName: string): Boolean;
    //��������� ��� ������� � ������� ������ ������!
    procedure ExecList;
    //�������� ������ ������!
    procedure ClearList;
  published
    //����������� ��� ������������ �������. ��-��������� = ;;
    property CommandDelimiter: string read FCommandDelimiter write
      SetCommandDelimiter;
    //���������� ������ � ������� �����. �������� ��� ��������� ������.
    property CommandsCount: integer read FGetCommandCount;
  end;

  {
    ����� ��� �������� ������������ ������ � �� ����������!
    ����: cmd param1 param2 "pa ra m 3" param4
  }
  TCmdParser = class
  private
    _full_line: string;
    params: TStringList;
    function GetParamsCount: integer;
  public
    constructor Create(ACmdLine: string = '');
    //���������� ������ � �������� � �����������
    procedure ParseCmdLine(ACmdLine: string = '');
    //���������� �������� �� ��� �������. ��������� ���� ����� ����� � �������
    function Param(param_index: Integer): string;
    //����� ��� Param
    function p(param_index: Integer): string;
    //���������� ��� ��������� (����� ��������, ������� � ���� cmd)
    function AllParams: string;
    //true, ���� acmd = Param(0), �� ���� ������� �������!
    function cmd(acmd: string): Boolean; overload;
    //true, ���� ���� �� ���� �� ���������� acmd �������
    function cmd(acmd: array of string): Boolean; overload;
    //������ ��������� ������� ��������� �� �������
    function ParamExists(param_index: integer): Boolean;
  published
    //���������� ����������
    property ParamsCount: integer read GetParamsCount;
  end;

var
  cmd_debug: boolean = false;

implementation

{ TCommandListParser }

procedure TCommandListParser.AddCommand(cmd_string: string);
begin
  FCommandList.Add(replaceDelimiter(cmd_string));
end;

procedure TCommandListParser.ClearList;
begin
  FCommandList.Clear;
end;

constructor TCommandListParser.Create(ACommandList: string = ''; varsManager:
  TVarReplacer = nil);
begin
  //����������� ��-���������!
  FCommandDelimiter := ';;';
  FCommandList := TStringList.Create;
  FCommandList.Text := ACommandList;

  if (varsManager <> nil) then
    VarManager := varsManager
  else
    VarManager := TVarReplacer.Create();
end;

procedure TCommandListParser.ExecList;
var
  i: Integer;
  cmd: string;
  Executor: TCommandExecutor;
begin
  Executor := TCommandExecutor.Create(self.VarManager);
  for i := 0 to FCommandList.Count - 1 do
  begin
    Executor.ExecCommand(Trim(FCommandList.Strings[i]));
  end;
  Executor.Free;
end;

function TCommandListParser.FGetCommandCount: integer;
begin
  result := FCommandList.Count;
end;

function TCommandListParser.LoadFromFile(AFileName: string): Boolean;
begin
  result := false;
  if (not FileExists(AFileName)) then
    exit;
  FCommandList.LoadFromFile(AFileName);
  ParseCommandList(FCommandList.text);
  result := true;
end;

procedure TCommandListParser.ParseCommandList(ACommandList: string);
begin
  FCommandList.Text := replaceDelimiter(ACommandList);
end;

function TCommandListParser.replaceDelimiter(from_text: string): string;
begin
  result := AnsiReplaceStr(from_text, FCommandDelimiter, #13#10);
end;

procedure TCommandListParser.SetCommandDelimiter(const Value: string);
begin
  FCommandDelimiter := Value;
end;

{ TCommandExecutor }

constructor TCommandExecutor.Create(VarReplacer: TVarReplacer = nil);
begin
  __utils := TUtils.Create;
  if (VarReplacer <> nil) then
    varManager := VarReplacer
  else
    varManager := TVarReplacer.Create();
end;

destructor TCommandExecutor.Destroy;
begin
  __utils.Free;
end;

function TCommandExecutor.ExecCommand(ACommand: string): boolean;
var
  cmdp: TCmdParser;
  s: string;

  function p(param_index: integer; default_value: string = ''): string;
    //����� ����� � ����������� ����������� ���� ��� ���������
  begin
    result := varManager.ReplaceVars(cmdp.p(param_index));
    if (result = '') then
      result := default_value;
  end;

  function pall(): string;
  begin
    result := trim(varManager.ReplaceVars(cmdp.AllParams));
  end;

begin
  ACommand := Trim(ACommand);
  if IsEmpty(ACommand) then
    Exit;
  //������ ��������� ������ �� ������� � ���������
  cmdp := TCmdParser.Create(ACommand);

  if (cmd_debug) then
    __utils.Writeln('[cmd]: ' + varManager.ReplaceVars(ACommand));

  if (cmdp.cmd('echo')) then
  begin
    __utils.Writeln(pall);
    exit;
  end;

  if (cmdp.cmd('debug_on')) then
  begin
    cmd_debug := True;
    Exit;
  end;

  if (cmdp.cmd('debug_off')) then
  begin
    cmd_debug := False;
    exit;
  end;

  if (cmdp.cmd('chdir')) then
  begin
    SetCurrentDir(p(1));
    Exit;
  end;

  if (cmdp.cmd('show_info')) then
  begin
    __utils.ShowInfo(p(1), p(2));
    exit;
  end;

  if (cmdp.cmd(['show_alert', 'show_warning'])) then
  begin
    __utils.ShowWarning(p(1), p(2));
    exit;
  end;

  if (cmdp.cmd('show_error')) then
  begin
    __utils.ShowError(p(1), p(2));
    exit;
  end;

  if (cmdp.cmd('cmd')) then
  begin
    WinExec(PChar(pall()), SW_RESTORE);
    Exit;
  end;

  if (cmdp.cmd('cmh')) then
  begin
    WinExec(PChar(pall()), SW_HIDE);
    Exit;
  end;

  if (cmdp.cmd('exec')) then
  begin
    __utils.ExecAndWait(pall);
    exit;
  end;

  if (cmdp.cmd('exeh')) then
  begin
    __utils.ExecAndWait(pall, SW_HIDE);
    exit;
  end;

  if (cmdp.cmd('halt')) then
  begin
    halt;
  end;

  if (cmdp.cmd('delete')) then
  begin
    __utils.Writeln('Deleting file "' + p(1) + '"...');
    if DeleteFile(p(1)) then
      __utils.Writeln('File deleted!')
    else
      __utils.Writeln('Error when deleting file...');
  end;

  if (cmdp.cmd('pause')) then
  begin
    __utils.Write('Paused! Press Enter to continue...');
    Readln;
    exit;
  end;

  if (cmdp.cmd(['sleep', 'delay'])) then
  begin
    //�������� � ��������� ���������� �����������, ��� �� 10, ���� �� �������
    Sleep(StrToInt(p(1, '10')));
    exit;
  end;

  { 18.05.017: ������ � ����������� � .. ������� � ����������� ����� ������...) }

  if (cmdp.cmd('write_info')) then
  begin
    __utils.WriteInfo(pall);
    exit;
  end;

  if (cmdp.cmd('write_error')) then
  begin
    __utils.WriteError(pall);
    exit;
  end;

  if (cmdp.cmd('set')) then // �������� ����������. ���������: set var value
  begin
    if (cmdp.ParamsCount < 2) then
    begin
      PrintCommandError(ACommand, 'set ����� ��� ������� 1 ��������!');
    end;

    varManager.AddVar(p(1), p(2));
    if (cmd_debug) then
      __utils.Writeln('[set var]: ' + p(1) + '=' + p(2));
    exit;
  end;

  if (cmdp.cmd('unset')) then // ������� ����������. ���������: unset var
  begin
    if (cmdp.ParamsCount < 2) then
      PrintCommandError(ACommand, 'unset ����� ������� �������� ����������!');
    varManager.DeleteParam(p(1));
    Exit;
  end;

  // �������� ���������� �����. ���������: file_get_contents file_path
  // ���������� ����� ����� � ���������� result
  if (cmdp.cmd('file_get_contents')) then
  begin
    if (cmdp.ParamsCount < 2) then
      PrintCommandError(ACommand, '���������� ������� ��� ������������� �����');
    if (not FileExists(p(1))) then
      PrintCommandError(ACommand, '���� "' + P(1) + '" �� ����������!');
    varManager.AddVar('result', __utils.FileGetContents(p(1)));
  end;

  if (cmdp.cmd('file_put_contents')) then
  begin
    if (cmdp.ParamsCount < 3) then
      PrintCommandError(ACommand,
        '���������� ������� ��� ����� � ������� ��� ������.');
    varManager.AddVar('result', IntToStr(__utils.FilePutContents(p(1), p(2))));
  end;

  {
  // 22.05.017: ��������, ������ ��� ���������� ��������!
  if (cmdp.cmd('http_get_text')) then
  begin
    if (cmdp.ParamsCount < 2) then
      PrintCommandError(ACommand,
        '���������� ������� URL ����� ��� GET-�������.');
    s := __utils.HttpGetText(p(1));
    if (s = TEXT_FALSE) then
      PrintCommandError(ACommand, 'http ������ ������-�� �� ������ :(');
    varManager.AddVar('result', s);
  end;
  }

  // ����� ���� ���� �� �����. ���������: find_file dir mask
  // � ������ ���������� �����, ��� ��� ����� � ���������� result
  if (cmdp.cmd('find_file')) then
  begin
    if (cmdp.ParamsCount < 3) then
      PrintCommandError(ACommand,
        '��� find_file ���������� ������� 2 ���������. ���� � ����� ����� ��� ������');

    varManager.AddVar('result', __utils.FindOneFile(p(1) + p(2)));
    exit;
  end;

  // ����� ��������� � ������, ��������� ���������� ���������.
  // ���������: ereg_str string ereg
  // ��������� ����� � ���������� result
  if (cmdp.cmd('ereg_str')) then
  begin
    if (cmdp.ParamsCount < 3) then
      PrintCommandError(ACommand,
        '���������� ������� ������ � ���������� ���������');

    varManager.AddVar('result', __utils.EregStr(P(1), P(2), StrToInt(P(3,
      '1'))));
    exit;
  end;
end;

procedure TCommandExecutor.PrintCommandError(command, error_text: string;
  terminate: Boolean = true);
begin
  __utils.Writeln('[Cmd Error]: ������ � ������� "' + command + '"');
  __utils.Writeln('[ErrorText]: ' + error_text);
  if (terminate) then
  begin
    __utils.Writeln('��������� ����� ���������. ������� Enter.');
    Readln;
    halt;
  end;
end;

{ TVarReplacer }

procedure TVarReplacer.AddVar(var_name, var_value: string);
begin
  AddStr(var_name, var_value);
end;

constructor TVarReplacer.Create(lines: TStrings);
begin
  FOpenChar := '$';
  FCloseChar := '$';
  inherited Create(lines);
end;

function TVarReplacer.GetVar(var_name, default_value: string): string;
begin
  result := GetStr(var_name, default_value);
end;

function TVarReplacer.ReplaceVars(input_string: string): string;
var
  i: integer;
  u: TUtils;

  procedure Replace(var_name, var_value: string);
  begin
    result := AnsiReplaceText(result, FOpenChar + var_name + FCloseChar,
      var_value);
  end;
begin
  result := input_string;
  if (input_string = '') then
    Exit;
  u := TUtils.Create;

  Replace('pid', IntToStr(GetCurrentProcessId));
  Replace('current_dir', GetCurrentDir);
  Replace('hostname', u.getHostName);
  Replace('username', u.getUserName);
  Replace('app_path', u.GetApplicationDir);
  Replace('program_dir', u.GetApplicationDir);
  Replace('paramstr(0)', ParamStr(0));
  Replace('rand', IntToStr(Random(99999)));

  for i := 0 to count - 1 do
    Replace(GetParamNameOfIndex(i), GetValueOfIndex(i));

  u.Free;
end;

procedure TVarReplacer.SetCloseChar(const Value: string);
begin
  FCloseChar := Value;
end;

procedure TVarReplacer.SetOpenChar(const Value: string);
begin
  FOpenChar := Value;
end;

{ TCmdParser }

function TCmdParser.cmd(acmd: string): Boolean;
begin
  result := (AnsiLowerCase(acmd) = AnsiLowerCase(Param(0)));
end;

function TCmdParser.AllParams: string;
var
  i: integer;
begin
  result := '';
  for i := 1 to params.Count - 1 do
    Result := result + ' ' + params.Strings[i];
end;

function TCmdParser.cmd(acmd: array of string): Boolean;
var
  i: integer;
begin
  result := true;
  for i := Low(acmd) to High(acmd) do
  begin
    if (cmd(acmd[i])) then
      exit;
  end;
  result := false;
end;

constructor TCmdParser.Create(ACmdLine: string);
begin
  params := TStringList.Create;
  if (ACmdLine <> '') then
    ParseCmdLine(ACmdLine);
end;

function TCmdParser.GetParamsCount: integer;
begin
  Result := params.Count;
end;

function TCmdParser.p(param_index: Integer): string;
begin
  result := Param(param_index);
end;

function TCmdParser.Param(param_index: Integer): string;
begin
  Result := '';
  if not ParamExists(param_index) then
    Exit;
  result := params.Strings[param_index];
end;

function TCmdParser.ParamExists(param_index: integer): Boolean;
begin
  result := (param_index < ParamsCount);
end;

procedure TCmdParser.ParseCmdLine(ACmdLine: string);
var
  i: integer;
  cc: Char; //current char
  current_param: string; //������������� ���������� ��� �������� ���������
  //���� ����������� �������, �� true, ���� ����� �� ���������� �� ��!
  quoted: Boolean;
  //���� ����������� �������, �� ��������� �, ���� �� ��������� ����!
  last_quote: Char;
  isLastChar: Boolean;
begin
  ACmdLine := Trim(ACmdLine);
  quoted := false;
  current_param := '';

  for i := 1 to Length(ACmdLine) do
  begin
    cc := ACmdLine[i];
    isLastChar := (i = Length(ACmdLine));

    //���� ����������� ������� ������� ��� ���������! (����!)
    if (cc = '"') or (cc = '''') then
    begin
      if (not quoted) then
      begin
        last_quote := cc;
        quoted := True;
        Continue;
      end;

      if (quoted) and (cc = last_quote) then
      begin
        quoted := false;
        if (not isLastChar) then
          Continue
        else
        begin //�������� ������. ���.
          params.Add(Trim(current_param));
          Continue;
        end;
      end;
    end;

    //�������� ������� ������ � �������� ���������
    current_param := current_param + cc;

    //��������� ������ ��� ��� ��������� ������!
    if (cc = ' ') or isLastChar then
    begin
      if (trim(current_param) = '') then
        Continue;

      if (not quoted) then
      begin
        params.Add(trim(current_param));
        current_param := '';
        Continue;
      end;
    end;
  end;
end;

end.

