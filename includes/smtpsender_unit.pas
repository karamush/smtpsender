unit smtpsender_unit;

interface

uses
  smtpsend, imapsend, classes, ssl_openssl, mimemess, mimepart, synautil,
  StrUtils, SysUtils, blcksock, utils, utf7Utils;

type
  TProxyType = (ptHTTP, ptSocks4, ptSocks5);
  TMessageType = (mtText, mtHTML);

  TOnSocketError = procedure(ErrCode: integer; ErrDescription: string) of
    object;
  TOnSMTPError = procedure(err: string; from: string = 'smtp') of object;
  TOnSMTPInfo = procedure(info: string; from: string = 'smtp') of object;

  TProxyParams = record
    Host, Port, User, Pass: string;
  end;

  TSMTPSender = class
  private
    smtp: TSMTPSend;
    imap: TIMAPSend;
    msg: TMimeMess;
    MIMEPart: TMimePart;
    //
    FSMTPHost: string;
    FSMTPPort: string;
    FOnSocketError: TOnSocketError;
    FSubject: string;
    FSendFrom: string;
    FSMTPUser: string;
    FSMTPPass: string;
    FUseProxy: Boolean;
    FProxyType: TProxyType;
    FProxyPass: string;
    FProxyHost: string;
    FProxyPort: string;
    FProxyUser: string;
    FUseSSL: Boolean;
    FSendTo: string;
    FBody: string;
    FMessageType: TMessageType;
    FOnSMTPError: TOnSMTPError;
    FOnSMTPInfo: TOnSMTPInfo;
    FSendCC: string;
    FSendBCC: string;
    FXMailer: string;
    FIMAPSave: Boolean;
    FIMAPPass: string;
    FIMAPFolder: string;
    FIMAPUser: string;
    FIMAPHost: string;
    FIMAPPort: string;
    function fillEmailsFromStr(emails: string): Boolean;
    procedure SetSMTPHost(const Value: string);
    procedure SetSMTPPort(const Value: string);
    procedure SetOnSocketError(const Value: TOnSocketError);
    procedure SetSendFrom(const Value: string);
    procedure SetSubject(const Value: string);
    procedure SetSMTPPass(const Value: string);
    procedure SetSMTPUser(const Value: string);
    procedure SetProxyType(const Value: TProxyType);
    procedure SetUseProxy(const Value: Boolean);
    procedure SetProxyHost(const Value: string);
    procedure SetProxyPass(const Value: string);
    procedure SetProxyPort(const Value: string);
    procedure SetProxyUser(const Value: string);
    procedure SetUseSSL(const Value: Boolean);
    procedure SetSendTo(const Value: string);
    procedure SetBody(const Value: string);
    procedure SetMessageType(const Value: TMessageType);
    procedure SetOnSMTPError(const Value: TOnSMTPError);
    procedure SetOnSMTPInfo(const Value: TOnSMTPInfo);
    procedure SetSendCC(const Value: string);
    procedure SetSendBCC(const Value: string);
    procedure SetXMailer(const Value: string);
    procedure SetIMAPFolder(const Value: string);
    procedure SetIMAPHost(const Value: string);
    procedure SetIMAPPass(const Value: string);
    procedure SetIMAPPort(const Value: string);
    procedure SetIMAPSave(const Value: Boolean);
    procedure SetIMAPUser(const Value: string);
    procedure setConnectionParams();
  public
    constructor create;
    destructor destroy;
    function getMessageTypeFromStr(mtString: string): TMessageType;
    function getProxyTypeFromStr(prString: string): TProxyType;
    function getProxyParamsFromStr(str: string): TProxyParams;
    //
    function AddAttachFile(FilePath: string; CustomFileName: string = ''):
      Boolean;
    function SendMessage(FakeSend: Boolean = false): Boolean;
    procedure SaveMessageToSentIfNeed();
    procedure CallOnSMTPError(err: string; from: string = 'smtp');
    procedure CallOnSMTPInfo(info: string; from: string = 'smtp');
    procedure printImapFoldersList();
  published
    property SMTPHost: string read FSMTPHost write SetSMTPHost;
    property SMTPPort: string read FSMTPPort write SetSMTPPort;
    property SMTPUser: string read FSMTPUser write SetSMTPUser;
    property SMTPPass: string read FSMTPPass write SetSMTPPass;

    property IMAPSave: Boolean read FIMAPSave write SetIMAPSave default false;
    property IMAPHost: string read FIMAPHost write SetIMAPHost;
    property IMAPPort: string read FIMAPPort write SetIMAPPort;
    property IMAPUser: string read FIMAPUser write SetIMAPUser;
    property IMAPPass: string read FIMAPPass write SetIMAPPass;
    property IMAPFolder: string read FIMAPFolder write SetIMAPFolder;

    property UseSSL: Boolean read FUseSSL write SetUseSSL default false;
    property SendFrom: string read FSendFrom write SetSendFrom;
    property SendTo: string read FSendTo write SetSendTo;
    property SendCC: string read FSendCC write SetSendCC;
    property SendBCC: string read FSendBCC write SetSendBCC;
    property Subject: string read FSubject write SetSubject;
    property Body: string read FBody write SetBody;
    //
    property UseProxy: Boolean read FUseProxy write SetUseProxy default false;
    property ProxyType: TProxyType read FProxyType write SetProxyType default
      ptHTTP;
    property ProxyHost: string read FProxyHost write SetProxyHost;
    property ProxyPort: string read FProxyPort write SetProxyPort;
    property ProxyUser: string read FProxyUser write SetProxyUser;
    property ProxyPass: string read FProxyPass write SetProxyPass;
    //
    property MessageType: TMessageType read FMessageType write SetMessageType
      default mtText;
    property XMailer: string read FXMailer write SetXMailer;
    //
    property OnSocketError: TOnSocketError read FOnSocketError write
      SetOnSocketError;
    property OnSMTPError: TOnSMTPError read FOnSMTPError write SetOnSMTPError;
    property OnSMTPInfo: TOnSMTPInfo read FOnSMTPInfo write SetOnSMTPInfo;
  end;

function SendMail(Host, Subject, pTo, From, TextBody, HTMLBody, login,
  password: string): Boolean;

var
  u: TUtils;

implementation

function SendMail(Host, Subject, pTo, From, TextBody, HTMLBody, login,
  password: string): Boolean;
var
  Msg: TMimeMess;
  StringList: TStringList;
  MIMEPart: TMimePart;
begin
  Msg := TMimeMess.Create;
  StringList := TStringList.Create;
  try
    Msg.Header.Subject := Subject; //���� ���������
    Msg.Header.From := From; //��� � ����� �����������
    Msg.Header.ToList.Add(pTo); //��� � ����� ����������
    // ������� �������� �������
    MIMEPart := Msg.AddPartMultipart('mixed', nil);
    if length(TextBody) > 0 then
    begin
      StringList.Text := TextBody;
      Msg.AddPartText(StringList, MIMEPart);
    end
    else
    begin
      StringList.Text := HTMLBody;
      Msg.AddPartHTML(StringList, MIMEPart);
    end;
    // �������� � ����������
    Msg.EncodeMessage;
    result := smtpsend.SendToRaw(From, pTo, Host, Msg.Lines, login, password);
  finally
    Msg.Free;
    StringList.Free;
  end;
end;

{ TSMTPSender }

function TSMTPSender.AddAttachFile(FilePath: string; CustomFileName: string =
  ''): Boolean;
var
  tmp: TMemoryStream;
begin
  result := false;
  if (not FileExists(FilePath)) then
    exit;
  tmp := TMemoryStream.Create;
  try
    tmp.LoadFromFile(FilePath);
    if (CustomFileName = '') then
      CustomFileName := ExtractFileName(FilePath);
    msg.AddPartBinary(tmp, CustomFileName, MIMEPart);
  finally
    tmp.Free;
  end;
  result := true;
end;

procedure TSMTPSender.CallOnSMTPError(err: string; from: string = 'smtp');
begin
  if Assigned(OnSMTPError) then
    OnSMTPError(err, from);
end;

procedure TSMTPSender.CallOnSMTPInfo(info: string; from: string = 'smtp');
begin
  if Assigned(FOnSMTPInfo) then
    FOnSMTPInfo(info, from);
end;

constructor TSMTPSender.create;
begin
  smtp := TSMTPSend.Create;
  imap := TIMAPSend.Create;
  msg := TMimeMess.Create;
  MIMEPart := TMimePart.Create;
  MIMEPart := Msg.AddPartMultipart('mixed', nil);
  //
  SMTPHost := '';
  SMTPPort := '25';
  SMTPUser := '';
  SMTPPass := '';
  Subject := '';
  SendFrom := '';
  //
  ProxyHost := '';
  ProxyPort := '3128';
  ProxyUser := '';
  ProxyPass := '';
  //
  XMailer := '';
  //
  IMAPPort := '993';
  IMAPFolder := 'Sent';
end;

destructor TSMTPSender.destroy;
begin
  MIMEPart.Free;
  msg.Free;
  smtp.Free;
end;

function TSMTPSender.fillEmailsFromStr(emails: string): Boolean;
var
  t: string;
begin
  Result := true;
  repeat
    t := GetEmailAddr(Trim(FetchEx(emails, ',', '"')));
    if t <> '' then
      Result := SMTP.MailTo(t);
    if not Result then
      Break;
  until emails = '';
end;

procedure TSMTPSender.printImapFoldersList();
var
  folders: TStringList;
  i: integer;
  folder, decodedFolder: string;
begin
  setConnectionParams();

  if not imap.Login() then
  begin
    CallOnSMTPError('�� ������� ������������! �������: ' + imap.ResultString,
      'imap');
    Exit;
  end;

  folders := TStringList.Create;
  if (not imap.List('', folders)) then
  begin
    CallOnSMTPError('�� ������� �������� ������ �����: ' + imap.ResultString,
      'imap');
    Exit;
  end;

  CallOnSMTPInfo('������ ����� �� �������� �������:', 'imap');

  u.Writeln(Format('    %-30s %s', ['��������', 'RAW']));
  for i := 0 to folders.Count - 1 do
  begin
    folder := folders[i];
    decodedFolder := IMAPModifiedUTF7Decode(folders[i]);

    u.Writeln(Format('  + %-30s %s', [decodedFolder, folder]));
  end;
  folders.Free;

  u.Writeln('');
  CallOnSMTPInfo('��� ��������� imap:folder ����� ��������� ��� ������ ��������, ��� � raw-��������.', 'imap');
  u.Writeln('');
end;

function TSMTPSender.getMessageTypeFromStr(mtString: string): TMessageType;
begin
  result := mtText;
  if (mtString = 'html') then
    result := mtHTML;
end;

function TSMTPSender.getProxyParamsFromStr(str: string): TProxyParams;
var
  s: string;
begin
  if (Pos('@', str) <> 0) then
  begin
    s := SeparateLeft(str, '@');
    result.User := SeparateLeft(s, ':');
    result.Pass := SeparateRight(s, ':');
    s := SeparateRight(str, '@');
  end
  else
    s := str;
  result.Host := SeparateLeft(s, ':');
  result.Port := SeparateRight(s, ':');
end;

function TSMTPSender.getProxyTypeFromStr(prString: string): TProxyType;
begin
  result := ptHTTP;
  if (prString = 'socks4') then
    result := ptSocks4;
  if (prString = 'socks5') then
    result := ptSocks5;
end;

procedure TSMTPSender.SaveMessageToSentIfNeed();
var
  folder: string;
begin
  if not FIMAPSave then
  begin
    CallOnSMTPInfo('���������� ���������, ������� ���������� ����������...',
      'imap');
    exit;
  end;

  if not imap.Login() then
  begin
    CallOnSMTPError('�� ������� ������������: ' + imap.ResultString, 'imap');
    Exit;
  end;

  CallOnSMTPInfo('������������ � ��������������!', 'imap');

  folder := IMAPModifiedUTF7Encode(FIMAPFolder);

  if (not imap.AppendMess(folder, msg.Lines)) then
  begin
    CallOnSMTPError('�� ������� ��������� ������ � ����� ' + FIMAPFolder +
      ' (utf7: ' + folder + '): ' + imap.ResultString, 'imap');
    exit;
  end;

  CallOnSMTPInfo('������ ������� ��������� � ����� ' + FIMAPFolder + ' (utf7: ' +
    folder + ')', 'imap');

  imap.Logout();
  CallOnSMTPInfo('�����������', 'imap');
end;

function TSMTPSender.SendMessage(FakeSend: Boolean = false): Boolean;
var
  msgBody: TStringList;
begin
  msgBody := TStringList.Create;
  try
    Msg.Header.Subject := Subject;
    Msg.Header.From := SendFrom;
    Msg.Header.XMailer := XMailer;

    // ����� ��������� ��������� ��������.
    Msg.Header.ToList.DelimitedText := SendTo;
    // � �����, ���� ����
    if (SendCC <> '') then
      Msg.Header.CCList.DelimitedText := SendCC;
    // � ������� ����� ����, ���� ���!
    if (SendBCC <> '') then
      Msg.Header.CustomHeaders.Add('BCC: ' + SendBCC);

    msgBody.Text := Body;
    if (MessageType = mtText) then
      Msg.AddPartText(msgBody, MIMEPart)
    else
      Msg.AddPartHTML(msgBody, MIMEPart);

    Msg.EncodeMessage;

    CallOnSMTPInfo('������ ������ ������ ���������� ' +
      IntToStr(Length(msg.Lines.Text)) + ' ����.');

    // �� ���������� ������ ����������.
    if (FakeSend) then
    begin
      CallOnSMTPInfo('������ ������ ���� ���� ������� ����������, �� �������� ��������� ���������� nosend.');
      result := True;
      exit;
    end;

    setConnectionParams();

    if (smtp.Login) then // and smtp.AuthDone
    begin
      CallOnSMTPInfo('������������ � ��������������!');

      if SMTP.MailFrom(GetEmailAddr(SendFrom), Length(msg.Lines.Text)) then
      begin
        CallOnSMTPInfo('�������� ������...');

        result := fillEmailsFromStr(SendTo);
        if (Result) then
          result := fillEmailsFromStr(SendCC);
        if (Result) then
          Result := fillEmailsFromStr(SendBCC);

        if Result then
          Result := SMTP.MailData(msg.Lines);
      end;

      if (result) then
      begin
        CallOnSMTPInfo('������ ������� ����������!');
        SaveMessageToSentIfNeed();
      end
      else
      begin
        CallOnSMTPError('������ ��� �������� ������. �������: ' +
          smtp.ResultString + ' -- ' + smtp.EnhCodeString);
      end;
      SMTP.Logout;
      CallOnSMTPInfo('�����������.');
    end
    else
    begin
      if (smtp.Sock.LastErrorDesc <> '') then
        CallOnSMTPError('�� ������� ������������ � smtp-�������. ��������� ����� ������� � ��������� ������(���� ������������ ��). SocketError: '
          + smtp.Sock.LastErrorDesc)
      else
        CallOnSMTPError('�� ������� ������������ � smtp-�������. ��������� ����� � ���� �������, ��������� ������(���� ������������ ��), � ��� �� ������� ������.');
    end;

    //result := smtpsend.SendToRaw(From, pTo, Host, Msg.Lines, login, password);
  finally
    msgBody.Free;
  end;
end;

procedure TSMTPSender.SetBody(const Value: string);
begin
  FBody := Value;
end;

procedure TSMTPSender.setConnectionParams;
begin
  if (UseProxy) then
  begin
    with smtp.Sock, imap.Sock do
    begin
      if (ProxyType = ptHTTP) then
      begin
        HTTPTunnelIP := ProxyHost;
        HTTPTunnelPort := ProxyPort;
        HTTPTunnelUser := ProxyUser;
        HTTPTunnelPass := ProxyPass;
      end
      else
      begin
        if (ProxyType = ptSocks4) then
          SocksType := ST_Socks4
        else
          SocksType := ST_Socks5;
        SocksIP := ProxyHost;
        SocksPort := ProxyPort;
        SocksUsername := ProxyUser;
        SocksPassword := ProxyPass;
        SocksResolver := True;
      end;
    end;
  end; //  ./if use proxy

  smtp.AutoTLS := True;
  smtp.FullSSL := UseSSL;
  smtp.TargetHost := SMTPHost;
  smtp.TargetPort := SMTPPort;
  smtp.UserName := SMTPUser;
  smtp.Password := SMTPPass;

  imap.TargetHost := FIMAPHost;
  imap.TargetPort := FIMAPPort;
  imap.UserName := FIMAPUser;
  imap.Password := FIMAPPass;
  imap.AutoTLS := True;
  imap.FullSSL := UseSSL;
end;

procedure TSMTPSender.SetIMAPFolder(const Value: string);
begin
  FIMAPFolder := Value;
end;

procedure TSMTPSender.SetIMAPHost(const Value: string);
begin
  FIMAPHost := Value;
end;

procedure TSMTPSender.SetIMAPPass(const Value: string);
begin
  FIMAPPass := Value;
end;

procedure TSMTPSender.SetIMAPPort(const Value: string);
begin
  FIMAPPort := Value;
end;

procedure TSMTPSender.SetIMAPSave(const Value: Boolean);
begin
  FIMAPSave := Value;
end;

procedure TSMTPSender.SetIMAPUser(const Value: string);
begin
  FIMAPUser := Value;
end;

procedure TSMTPSender.SetMessageType(const Value: TMessageType);
begin
  FMessageType := Value;
end;

procedure TSMTPSender.SetOnSMTPError(const Value: TOnSMTPError);
begin
  FOnSMTPError := Value;
end;

procedure TSMTPSender.SetOnSMTPInfo(const Value: TOnSMTPInfo);
begin
  FOnSMTPInfo := Value;
end;

procedure TSMTPSender.SetOnSocketError(const Value: TOnSocketError);
begin
  FOnSocketError := Value;
end;

procedure TSMTPSender.SetProxyHost(const Value: string);
begin
  FProxyHost := Value;
end;

procedure TSMTPSender.SetProxyPass(const Value: string);
begin
  FProxyPass := Value;
end;

procedure TSMTPSender.SetProxyPort(const Value: string);
begin
  FProxyPort := Value;
end;

procedure TSMTPSender.SetProxyType(const Value: TProxyType);
begin
  FProxyType := Value;
end;

procedure TSMTPSender.SetProxyUser(const Value: string);
begin
  FProxyUser := Value;
end;

procedure TSMTPSender.SetSendBCC(const Value: string);
begin
  FSendBCC := Value;
end;

procedure TSMTPSender.SetSendCC(const Value: string);
begin
  FSendCC := Value;
end;

procedure TSMTPSender.SetSendFrom(const Value: string);
begin
  FSendFrom := Value;
end;

procedure TSMTPSender.SetSendTo(const Value: string);
begin
  FSendTo := Value;
end;

procedure TSMTPSender.SetSMTPHost(const Value: string);
begin
  FSMTPHost := Value;
end;

procedure TSMTPSender.SetSMTPPass(const Value: string);
begin
  FSMTPPass := Value;
end;

procedure TSMTPSender.SetSMTPPort(const Value: string);
begin
  FSMTPPort := Value;
end;

procedure TSMTPSender.SetSMTPUser(const Value: string);
begin
  FSMTPUser := Value;
end;

procedure TSMTPSender.SetSubject(const Value: string);
begin
  FSubject := Value;
end;

procedure TSMTPSender.SetUseProxy(const Value: Boolean);
begin
  FUseProxy := Value;
end;

procedure TSMTPSender.SetUseSSL(const Value: Boolean);
begin
  FUseSSL := Value;
end;

procedure TSMTPSender.SetXMailer(const Value: string);
begin
  FXMailer := Value;
end;

end.

