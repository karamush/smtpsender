unit utf7Utils;

interface

uses Classes, SysUtils, Windows;

const
  Base64Table: array[0..63] of AnsiChar =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

type
  TBytes = array of Byte;

function IMAPModifiedUTF7Decode(const S: AnsiString): WideString;
function IMAPModifiedUTF7Encode(const WS: WideString): AnsiString;

implementation

function Base64CharToValue(ch: AnsiChar): Integer;
begin
  case ch of
    'A'..'Z': Result := Ord(ch) - Ord('A');
    'a'..'z': Result := Ord(ch) - Ord('a') + 26;
    '0'..'9': Result := Ord(ch) - Ord('0') + 52;
    '+': Result := 62;
    '/': Result := 63;
  else
    Result := -1;
  end;
end;

function DecodeBase64(const Input: AnsiString): TBytes;
var
  i, j, Len: Integer;
  v: LongWord;
  OutLen: Integer;
begin
  Len := Length(Input);
  OutLen := (Len * 3) div 4;
  SetLength(Result, OutLen);
  i := 1;
  j := 0;
  while i <= Len - 3 do
  begin
    v := (Base64CharToValue(Input[i]) shl 18) or
      (Base64CharToValue(Input[i + 1]) shl 12) or
      (Base64CharToValue(Input[i + 2]) shl 6) or
      (Base64CharToValue(Input[i + 3]));
    Inc(i, 4);
    if j < OutLen then
    begin
      Result[j] := (v shr 16) and $FF;
      Inc(j);
    end;
    if j < OutLen then
    begin
      Result[j] := (v shr 8) and $FF;
      Inc(j);
    end;
    if j < OutLen then
    begin
      Result[j] := v and $FF;
      Inc(j);
    end;
  end;
end;

function UTF16BEToWideString(const Bytes: TBytes): WideString;
var
  i, Count: Integer;
  CodeUnit: Word;
begin
  Count := Length(Bytes) div 2;
  SetLength(Result, Count);
  for i := 0 to Count - 1 do
  begin
    CodeUnit := (Bytes[i * 2] shl 8) or Bytes[i * 2 + 1];
    Result[i + 1] := WideChar(CodeUnit);
  end;
end;

function IMAPModifiedUTF7Decode(const S: AnsiString): WideString;
var
  i, j: Integer;
  Part, Base64Str: AnsiString;
  Decoded: TBytes;
begin
  Result := '';
  i := 1;
  while i <= Length(S) do
  begin
    if S[i] = '&' then
    begin
      if (i < Length(S)) and (S[i + 1] = '-') then
      begin
        Result := Result + '&';
        Inc(i, 2);
      end
      else
      begin
        j := i + 1;
        while (j <= Length(S)) and (S[j] <> '-') do
          Inc(j);
        Part := Copy(S, i + 1, j - i - 1);
        Base64Str := StringReplace(Part, ',', '/', [rfReplaceAll]);
        Decoded := DecodeBase64(Base64Str);
        Result := Result + UTF16BEToWideString(Decoded);
        i := j + 1;
      end;
    end
    else
    begin
      Result := Result + WideChar(S[i]);
      Inc(i);
    end;
  end;
end;

function EncodeBase64(const Data: TBytes): AnsiString;
var
  i, Len: Integer;
  v: LongWord;
begin
  Result := '';
  Len := Length(Data);
  i := 0;
  while i < Len do
  begin
    v := Data[i] shl 16;
    if (i + 1 < Len) then
      v := v or (Data[i + 1] shl 8);
    if (i + 2 < Len) then
      v := v or Data[i + 2];
    Result := Result +
      Base64Table[(v shr 18) and $3F] +
      Base64Table[(v shr 12) and $3F];
    if (i + 1 < Len) then
      Result := Result + Base64Table[(v shr 6) and $3F]
    else
      Result := Result + '=';
    if (i + 2 < Len) then
      Result := Result + Base64Table[v and $3F]
    else
      Result := Result + '=';
    Inc(i, 3);
  end;
end;

function WideStringToUTF16BE(const WS: WideString): TBytes;
var
  i, Len: Integer;
  Code: Word;
begin
  Len := Length(WS);
  SetLength(Result, Len * 2);
  for i := 1 to Len do
  begin
    Code := Word(WS[i]);
    Result[(i - 1) * 2] := Code shr 8;
    Result[(i - 1) * 2 + 1] := Code and $FF;
  end;
end;

function IMAPModifiedUTF7Encode(const WS: WideString): AnsiString;
var
  i, Start, L: Integer;
  OutStr, Encoded: AnsiString;
  PartBytes: TBytes;
  Base64Str: AnsiString;
  ch: WideChar;
begin
  OutStr := '';
  Start := 1;
  L := Length(WS);
  for i := 1 to L do
  begin
    ch := WS[i];

    if (Ord(ch) >= 32) and (Ord(ch) <= 126) and (ch <> '&') then
    begin
      if i > Start then
      begin
        Encoded := EncodeBase64(WideStringToUTF16BE(Copy(WS, Start, i -
          Start)));
        Encoded := StringReplace(Encoded, '/', ',', [rfReplaceAll]);
        Encoded := StringReplace(Encoded, '=', '', [rfReplaceAll]);
        OutStr := OutStr + '&' + Encoded + '-';
      end;
      OutStr := OutStr + AnsiString(ch);
      Start := i + 1;
    end;
  end;
  if Start <= L then
  begin
    Encoded := EncodeBase64(WideStringToUTF16BE(Copy(WS, Start, L - Start +
      1)));
    Encoded := StringReplace(Encoded, '/', ',', [rfReplaceAll]);
    Encoded := StringReplace(Encoded, '=', '', [rfReplaceAll]);
    OutStr := OutStr + '&' + Encoded + '-';
  end;
  OutStr := StringReplace(OutStr, '&-', '&-&-', [rfReplaceAll]);
  Result := OutStr;
end;

end.

