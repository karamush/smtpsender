unit utils;

interface

uses
  Windows, SysUtils, synacode, StrUtils, classes, RegExpr;

const
  TEXT_FALSE = '##FALSE##';

type
  TUtils = class
  private
    lastParamValue: string;
    FErrorExists: Boolean;
    procedure SetErrorExists(const Value: Boolean);
  public
    //�������� ��������� ParamStr
    function p(index: Integer = 0): string;
    //���������� ����������
    function pcount: integer;
    //���������� �� ��������� ��������
    function paramExists(param: string): Boolean; overload;
    //���������� �� ���� ���� �� ��������� ����������
    function paramExists(s: array of string): Boolean; overload;
    //�������� �������� ��������� (������ ��������� �� �����)
    function getParamValue(param: string): string;
    //������ ������ ���������� ���������
    function getParamIndex(param: string): integer;
    //��������� ������ � oem, ����� � ������� �������� ���� ����������
    function StrAnsiToOem(const S: AnsiString): AnsiString;
    //�� ��, ��� � ������ writeln, �� ����� �������� ��������
    procedure Writeln(s: string);
    //�� ��, ��� � ������ write, �� ����� �������� ��������
    procedure write(s: string);
    //������� ������.
    procedure WriteError(s: string);
    //������� ����-���������
    procedure WriteInfo(s: string);
    //�������� ��� �������� ������������ �����
    function getMyExeName: string;
    function GetApplicationDir: string;
    //���������� �������� ���������, ������� ��� ������� ���
    //������ ������� paramExists
    function getLastParamValue: string;
    //������ true, ���� str1 ���������� ��������� str2
    function compareWithLenght(str1, str2: string): Boolean;
    //����������� ������
    function cryptPass(str: string): string;
    //������������ ������������� ������
    function decryptPass(str: string): string;
    //
    function getTextFromFile(AFile: string): string;
    //
    //��������� 03.04.017!
    function ExecAndWait(exec_str: string; show_window: Cardinal = SW_RESTORE):
      Boolean;
    function getHostName(): string;
    function getUserName(): string;
    //messages
    procedure ShowInfo(text: string; title: string = '����������');
    procedure ShowWarning(text: string; title: string = '��������!');
    procedure ShowError(text: string; title: string = '������!');
    function ShowConfirm(text: string; title: string = '������!'): boolean;
    // 18.05.017 -- ����� ������ ����� �� �����
    function FindOneFile(path_mask: string): string;
    // �������� ���������� ���������� ���������� �����
    function FileGetContents(file_name: string): string;
    // �������� ����� ����� � ����� ����! � ������� ���������� ���������� ����.
    function FilePutContents(file_name, text: string): integer;
    // ����� ����� ������ �� ������ �� ����������� ���������. ������� ���� ���������� (math_id -- ������)
    function EregStr(str, ereg: string; math_id: integer = 1): string;
    // ������� ������ � �������� ������� �� ���������� URL ������
    function HttpGetText(URL: string): string;
  published
    property ErrorExists: Boolean read FErrorExists write SetErrorExists default
      false;
  end;

  { ���� ����� ������ �� ������� ������� (������, �������), ������� ��� ���� ���������
    �������� � ������ �����. �� ������ }
  TParamParser = class
  private
    str: TStrings;
    function AddParam(ParamName: string; ParamValue: string): boolean;
    function GetParam(ParamName: string; default_value: string = ''): string;
    function GetCount: integer;
  public
    ParamDelimiter: string;
    PDelimiter: string;
    constructor Create(lines: TStrings = nil);
    destructor destroy; override;
    function ParamExists(ParamName: string): boolean;
    function GetParamIndex(ParamName: string): integer;
    function GetValueIndex(Value: string): integer;
    function UpdateParam(ParamName: string; NewValue: string): boolean;
    function GetAllText: string;
    function getStringsObject: TStrings; //added 18.05.017
    function DeleteParam(ParamName: string): boolean; overload;
    function DeleteParam(ParamIndex: integer): boolean; overload;
    function ExtractParamName(ParamString: string): string;
    //Copy of 0 to pos(ParamDelimiter)
    function ExtractParamValue(ParamString: string): string;
    function GetParamNameOfValue(value: string): string;
    function GetParamNameOfIndex(index: integer): string;
    function GetValueOfIndex(index: integer): string;
    //
    procedure AddRawLine(str: string);
    function AddStr(Param: string; Value: string = ''): boolean;
    function AddInt(Param: string; Value: integer = 0): boolean;
    function AddBool(Param: string; Value: boolean = false): boolean;
    function GetStr(ParamName: string; default_value: string = ''): string;
    function GetInt(ParamName: string; default_value: integer = 0): integer;
    function GetBool(ParamName: string; default_value: boolean = false):
      boolean;
    function isEmptyParam(ParamName: string): Boolean;
    //
    function GetParamsList: string;
    function GetValuesList: string;
    //
    function UnparseString(delimiter: string = '&'): string;
    procedure ParseString(InStr: string; delimiter: string = '&');
    //
    function LoadFromFile(AFileName: string): boolean;
    function SaveToFile(AFileName: string): boolean;
    //
    procedure SetAllText(text: string);
    procedure ClearAll;
  published
    property count: integer read GetCount;
  end;

function IsEmptyEx(InStr: string): boolean;
function IsEmpty(InStr: string): boolean;
function BoolToStr(abool: boolean): string;
function StrToBool(str: string): boolean;

const
  CRLF = #13#10;

implementation

{ TUtils }

function TUtils.compareWithLenght(str1, str2: string): Boolean;
begin
  str1 := AnsiLowerCase(str1);
  str2 := AnsiLowerCase(str2);
  result := Copy(str1, 0, Length(str2)) = str2;
end;

function TUtils.cryptPass(str: string): string;
begin
  result := EncodeBase64(ReverseString(EncodeBase64(ReverseString(str))));
end;

function TUtils.decryptPass(str: string): string;
begin
  result := ReverseString(DecodeBase64(ReverseString(DecodeBase64(str))));
end;

function TUtils.getLastParamValue: string;
begin
  result := '';
  //���������, � ����� ���� ������ �� �������� ���������, � ��������� �������?
  if ((lastParamValue <> '') and (lastParamValue[1] = '-')) then
    exit;
  result := lastParamValue;
end;

function TUtils.getMyExeName: string;
begin
  Result := ExtractFileName(p(0));
end;

function TUtils.getParamIndex(param: string): integer;
var
  i: integer;
begin
  result := -1;
  for i := 1 to pcount do
  begin
    if ((p(i) = param) or (p(i) = '-' + param)) then
      result := i;
    break;
  end;
end;

function TUtils.getParamValue(param: string): string;
var
  i: integer;
  np: string;
begin
  result := '';
  if (not paramExists(param)) then
    exit;
  np := p(getParamIndex(param) + 1); //next param
  if (np <> '') then
    result := np;
end;

function TUtils.getTextFromFile(AFile: string): string;
var
  s: TStringList;
begin
  result := '';
  if (not FileExists(AFile)) then
    Exit;
  s := TStringList.Create;
  s.LoadFromFile(AFile);
  result := s.Text;
  s.Free;
end;

function TUtils.p(index: Integer): string;
begin
  if (index > ParamCount) then
    result := ''
  else
    result := AnsiLowerCase(ParamStr(index));
  if (result <> '') then
    lastParamValue := ParamStr(index + 1);
end;

function TUtils.paramExists(param: string): Boolean;
var
  i: integer;
begin
  param := AnsiLowerCase(param);
  result := true;
  for i := 1 to pcount do
    if ((p(i) = param) or (p(i) = '-' + param)) then
    begin
      //��� ����� ����������� �������� ���������, ����� ����� ���� ����� �� ������
      lastParamValue := ParamStr(i + 1);
      exit;
    end;
  result := false;
end;

function TUtils.paramExists(s: array of string): Boolean;
var
  i, pi: integer;
  current_param, tmp_param: string;
begin
  result := true;
  for i := 1 to pcount do
  begin
    current_param := p(i);
    for pi := Low(s) to High(s) do
    begin
      tmp_param := AnsiLowerCase(s[pi]);
      if ((current_param = tmp_param) or (current_param = '-' + tmp_param)) then
      begin
        lastParamValue := ParamStr(i + 1);
        exit;
      end;
    end;
  end;
  result := false;
end;

function TUtils.pcount: integer;
begin
  result := ParamCount;
end;

procedure TUtils.SetErrorExists(const Value: Boolean);
begin
  FErrorExists := Value;
end;

function TUtils.StrAnsiToOem(const S: AnsiString): AnsiString;
begin
  SetLength(Result, Length(S));
  AnsiToOemBuff(@S[1], @Result[1], Length(S));
end;

procedure TUtils.write(s: string);
begin
  system.Write(StrAnsiToOem(s));
end;

procedure TUtils.WriteError(s: string);
begin
  self.Writeln('  [error]: ' + s);
  ErrorExists := True;
end;

procedure TUtils.WriteInfo(s: string);
begin
  self.Writeln('  [info]: ' + s);
end;

procedure TUtils.Writeln(s: string);
begin
  system.Writeln(StrAnsiToOem(s));
end;

function IsEmptyEx(InStr: string): boolean;
begin
  result := Length(trim(InStr)) = 0;
end;

function IsEmpty(InStr: string): boolean;
begin
  result := Length(InStr) = 0;
end;

function BoolToStr(abool: boolean): string;
begin
  result := IfThen(abool, 'true', 'false');
end;

function StrToBool(str: string): boolean;
begin
  str := trim(AnsiLowerCase(str));
  result := ((str = 'true') or (str = '1') or (str = 'on'));
end;

function TUtils.getHostName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

function TUtils.getUserName: string;
var
  UserName: string;
  UserNameLen: Dword;
begin
  UserNameLen := 255;
  SetLength(userName, UserNameLen);
  if windows.GetUserName(PChar(UserName), UserNameLen) then
    Result := Copy(UserName, 1, UserNameLen - 1)
  else
    Result := '[unknown]';
end;

procedure TUtils.ShowInfo(text, title: string);
begin
  title := IfThen(title = '', '����������', title);
  MessageBox(GetForegroundWindow, PChar(text), PChar(title), MB_ICONINFORMATION
    + MB_SETFOREGROUND + MB_TOPMOST)
end;

procedure TUtils.ShowError(text, title: string);
begin
  title := IfThen(title = '', '��������!', title);
  MessageBox(GetForegroundWindow, PChar(text), PChar(title), MB_ICONWARNING
    + MB_SETFOREGROUND + MB_TOPMOST)
end;

procedure TUtils.ShowWarning(text, title: string);
begin
  title := IfThen(title = '', '������!', title);
  MessageBox(GetForegroundWindow, PChar(text), PChar(title), MB_ICONHAND
    + MB_SETFOREGROUND + MB_TOPMOST)
end;

function TUtils.GetApplicationDir: string;
begin
  result := ExtractFileDir(ParamStr(0)) + '\';
end;

function TUtils.FindOneFile(path_mask: string): string;
var
  searchResult : TSearchRec;
begin
  result := '';
  if FindFirst(path_mask, faAnyFile, searchResult) = 0 then
    result := searchResult.Name;
  FindClose(searchResult);
end;

function TUtils.FileGetContents(file_name: string): string;
var
  s: TStringList;
begin
  result := '';
  if (not FileExists(file_name)) then
    Exit;
  s := TStringList.Create;
  s.LoadFromFile(file_name);
  result := s.Text;
  s.Free;
end;

function TUtils.EregStr(str, ereg: string; math_id: integer = 1): string;
var
  r: TRegExpr;
begin
  result := '';
  r := TRegExpr.Create;
  r.Expression := ereg;
  r.ModifierI := true;  // ������������������
  //r.ModifierG := false; // ������������ ����� ������� ����������
  if (r.Exec(str)) then
    result := r.Match[math_id];
  r.Free;
end;

function TUtils.FilePutContents(file_name, text: string): integer;
var
  s: TStringList;
begin
  s := TStringList.Create;
  s.Text := text;
  s.SaveToFile(file_name);
  Result := Length(text);
  s.Free;
end;

function TUtils.HttpGetText(URL: string): string;
var
  s: TStringList;
begin
  // 22.05.017: ��������, ������ ��� ���������� ��������!
  result := TEXT_FALSE;
  {
  s := TStringList.Create;
  if not httpsend.HttpGetText(URL, s) then
    result := TEXT_FALSE
  else
    result := s.Text;
  s.Free;
  }
end;

function TUtils.ShowConfirm(text, title: string): boolean;
begin
  result := (MessageBox(GetForegroundWindow, PChar(text), PChar(title),
    MB_ICONQUESTION + MB_SETFOREGROUND + MB_TOPMOST + MB_YESNO) = IDYES);
end;

{ TParamParser }

function TParamParser.AddBool(Param: string; Value: boolean): boolean;
begin
  result := AddParam(Param, BoolToStr(Value));
end;

function TParamParser.AddInt(Param: string; Value: integer): boolean;
begin
  result := AddParam(Param, IntToStr(Value));
end;

function TParamParser.AddParam(ParamName, ParamValue: string): boolean;
begin
  result := false;
  if IsEmptyEx(paramName) then
    exit;
  if ParamExists(ParamName) then
    result := UpdateParam(ParamName, ParamValue)
  else
  begin
    str.Add(ParamName + ParamDelimiter + ParamValue);
    result := true;
  end;
end;

procedure TParamParser.AddRawLine(str: string);
begin
  self.str.Add(str);
end;

function TParamParser.AddStr(Param, Value: string): boolean;
begin
  result := AddParam(Param, Value);
end;

procedure TParamParser.ClearAll;
begin
  str.Clear;
end;

constructor TParamParser.Create(lines: TStrings = nil);
begin
  ParamDelimiter := '=';
  PDelimiter := '&';
  if lines = nil then
    str := TStringList.Create
  else
    str := lines;
end;

function TParamParser.DeleteParam(ParamName: string): boolean;
begin
  result := false;
  if not ParamExists(ParamName) then
    exit;
  str.Delete(GetParamIndex(ParamName));
  result := true;
end;

function TParamParser.DeleteParam(ParamIndex: integer): boolean;
begin
  try
    result := false;
    if ParamIndex > str.Count then
      exit;
    str.Delete(ParamIndex);
    result := true;
  except
  end;
end;

destructor TParamParser.destroy;
begin
  str.Free;
end;

function TUtils.ExecAndWait(exec_str: string; show_window: Cardinal =
  SW_RESTORE): Boolean;
var
  StartupInfo: _STARTUPINFOA;
  ProcInfo: _PROCESS_INFORMATION;
begin
  FillChar(StartupInfo, sizeof(StartupInfo), #0);
  StartupInfo.cb := sizeof(StartupInfo);
  StartupInfo.wShowWindow := show_window;
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  FillChar(StartupInfo, sizeof(ProcInfo), #0);
  result := CreateProcess(nil, PAnsiChar(exec_str), nil, nil, false, 0, nil,
    nil, StartupInfo, ProcInfo);
  if Result then
  begin
    while WaitForSingleObject(ProcInfo.hProcess, 1000) <> WAIT_OBJECT_0 do
    begin
      //write('.'); //mini progress bar :)
    end;
    CloseHandle(ProcInfo.hProcess);
    CloseHandle(ProcInfo.hThread);
  end;
end;

function TParamParser.ExtractParamName(ParamString: string): string;
begin
  if Pos(ParamDelimiter, ParamString) = 0 then
    ParamString := ParamString + ParamDelimiter;
  result := copy(ParamString, 0, pos(ParamDelimiter, ParamString) - 1);
end;

function TParamParser.ExtractParamValue(ParamString: string): string;
begin
  result := copy(ParamString, pos(ParamDelimiter, ParamString) + 1,
    length(ParamString));
end;

function TParamParser.GetAllText: string;
begin
  result := str.Text;
end;

function TParamParser.GetBool(ParamName: string; default_value: boolean =
  false): boolean;
begin
  result := StrToBool(GetParam(ParamName, BoolToStr(default_value)));
end;

function TParamParser.GetCount: integer;
begin
  result := str.Count;
end;

function TParamParser.GetInt(ParamName: string; default_value: integer = 0):
  integer;
begin
  try
    result := StrToInt(IfThen(ParamExists(ParamName), GetParam(ParamName),
      IntToStr(default_value)));
  except
  end;
end;

function TParamParser.GetParam(ParamName: string; default_value: string = ''):
  string;
begin
  if ParamExists(ParamName) then
    result := ExtractParamValue(str.Strings[GetParamIndex(ParamName)])
  else
  begin
    result := default_value;
    if not IsEmpty(default_value) then
      AddParam(ParamName, default_value);
  end;
  result := AnsiReplaceStr(result, #1#2#4#2, CRLF);
end;

function TParamParser.GetParamIndex(ParamName: string): integer;
var
  i: integer;
  s: string;
begin
  result := -1;
  ParamName := trim(AnsiLowerCase(ParamName));
  for i := 0 to str.Count - 1 do
  begin
    s := AnsiLowerCase(trim(str.Strings[i]));
    if ParamName = ExtractParamName(s) then
    begin
      result := i;
      break;
    end;
  end;
end;

function TParamParser.GetParamNameOfIndex(index: integer): string;
begin
  result := '';
  if index > str.Count then
    exit;
  result := ExtractParamName(str.Strings[index]);
end;

function TParamParser.GetParamNameOfValue(value: string): string;
begin
  result := ExtractParamName(str.Strings[GetValueIndex(value)]);
end;

function TParamParser.GetParamsList: string;
var
  i: integer;
begin
  result := '';
  for i := 0 to str.Count - 1 do
    result := result + CRLF + getParamNameOfIndex(i);
end;

function TParamParser.GetStr(ParamName: string; default_value: string = ''):
  string;
begin
  result := GetParam(ParamName, default_value);
end;

function TParamParser.getStringsObject: TStrings;
begin
  result := self.str;
end;

function TParamParser.GetValueIndex(Value: string): integer;
var
  i: integer;
  s: string;
begin
  result := -1;
  Value := trim(AnsiLowerCase(Value));
  for i := 0 to str.Count - 1 do
  begin
    s := AnsiLowerCase(trim(str.Strings[i]));
    if Value = ExtractParamValue(s) then
    begin
      result := i;
      break;
    end;
  end;
end;

function TParamParser.GetValueOfIndex(index: integer): string;
begin
  result := '';
  if index > str.Count then
    exit;
  result := ExtractParamValue(str.Strings[index]);
end;

function TParamParser.GetValuesList: string;
var
  i: integer;
begin
  result := '';
  for i := 0 to str.Count - 1 do
    result := result + CRLF + GetValueOfIndex(i);
end;

function TParamParser.isEmptyParam(ParamName: string): Boolean;
begin
  result := GetParam(ParamName) = '';
end;

function TParamParser.LoadFromFile(AFileName: string): boolean;
begin
  result := false;
  try
    if not FileExists(AFileName) then
      exit;
    str.LoadFromFile(AFileName);
  except
    exit;
  end;
  result := true;
end;

function TParamParser.ParamExists(ParamName: string): boolean;
begin
  result := GetParamIndex(ParamName) <> -1;
end;

procedure TParamParser.ParseString(InStr, delimiter: string);
begin
  if PDelimiter <> Delimiter then
    PDelimiter := Delimiter;
  InStr := AnsiReplaceStr(InStr, CRLF, #1#2#4#2);
  str.Text := AnsiReplaceText(InStr, PDelimiter, CRLF);
end;

function TParamParser.SaveToFile(AFileName: string): boolean;
begin
  result := false;
  try
    str.SaveToFile(AFileName);
    result := true;
  except
    result := false;
  end;
end;

procedure TParamParser.SetAllText(text: string);
begin
  str.Clear;
  str.Text := text;
end;

function TParamParser.UnparseString(delimiter: string): string;
begin
  {if PDelimiter <> Delimiter then
    PDelimiter := Delimiter;}
  result := AnsiReplaceText(str.Text, CRLF, PDelimiter);
end;

function TParamParser.UpdateParam(ParamName, NewValue: string): boolean;
begin
  result := ParamExists(ParamName);
  if not result then
    exit;
  str.Strings[GetParamIndex(ParamName)] := ParamName + ParamDelimiter +
    NewValue;
end;

end.

