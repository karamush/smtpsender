program smtpsender;

{$APPTYPE CONSOLE}

uses
  windows,
  SysUtils,
  StrUtils,
  synautil,
  classes,
  smtpsender_unit in 'includes\smtpsender_unit.pas',
  utils in 'includes\utils.pas',
  executor in 'includes\executor.pas',
  utf7Utils in 'includes\utf7Utils.pas';

var
  u: TUtils;
  smtp: TSMTPSender;
  sendParams: TParamParser; //��� ��������� �������� � ����� ����� ���
  varsManager: TVarReplacer;

type
  TSMTPErrorGetter = class
    constructor Create;
    procedure OnSocketError(ErrCode: integer; ErrDescription: string);
    procedure onSMTPError(err: string; from: string = 'smtp');
    procedure onSMTPInfo(info: string; from: string = 'smtp');
  end;

const
  version = '0.8';

  {*********************************************************}

procedure printHelp();
begin
  u.Writeln('�������������: ');
  u.writeln('   ' + u.getMyExeName +
    ' [-load_data <����>] -from <email> -to <email> ' + CRLF +
    '   [-cc <�����>] [-bcc <������� �����>][-subject <����>] -body <�����>');
  u.Writeln('   -load_body <����> [-attach <����[;���]>] [-type [text|html]]');
  u.writeln('   -server <hostname ��� IP> -port <port> [-user <���>]' +
    ' [-pass <������>]');
  u.Writeln('   [-cpass <������>] [-crpass <������>] [-ssl]');
  u.writeln('   [-imap:save] [-imap:server <host>] [-imap:port <port=993>]');
  u.writeln('   [-imap:user <user>] [-imap:pass <pass>] [-imap:folder <Folder=Sent>]');
  u.writeln('   [-imap:folders_list]');
  u.Writeln('   [-proxy [��� ������]] [-proxy_addr <[������������@]�����>]');
  u.Writeln('   [-before_exec <commands>] [-before_exec_file <filename>]');
  u.writeln('   [-after_exec <commands>] [-after_exec_file <filename>]');

  u.Writeln('');
  u.Writeln('���������: ');
  u.Writeln('');
  u.Writeln(' -h, -help                           �������� ��� ���������');
  u.Writeln(' -load_data  <����>                  ��������� ��������� �������� �� ����������');
  u.Writeln('                                     �����. �� ���������, �������� ����, ����� ');
  u.Writeln('                                     ����� ������� ���������. ���� ���� ��');
  u.Writeln('                                     ������ ����, �� ��������� ����������� ����');
  u.Writeln('                                     smtpsend.conf, ���� �� ���������� �');
  u.Writeln('                                     ����������, ��� ��������� ���� ���������');
  u.Writeln(' -make_data  <����>                  ������� �������� ���� ���������� ��������,');
  u.Writeln('                                     ����� ��������� � ������������ ���');
  u.Writeln(' -from       <email>                 ����� �����������');
  u.Writeln(' -to         <email>                 �����(a) ����������� (��������� ����� �������)');
  u.Writeln(' -cc         <email[s]>              ���� "�����". ����� ��� ������');
  u.Writeln('                                     ����������� ����� �������.');
  u.Writeln(' -bcc         <email[s]>             ���� "������� �����". ����� ��� ������');
  u.Writeln('                                     ����������� ����� �������.');
  u.Writeln(' -subject    <����>                  ���� ���������');
  u.Writeln(' -body       <�����>                 ����� ���������. \n - ��� ����� ������');
  u.Writeln(' -load_body  <����>                  ��������� ����� ��������� �� �����');
  u.Writeln(' -attach     <����[;��� �����]>      ��������� ����. ����� �������');
  u.Writeln('                                     ������������ ��� �����. ��� �� �����');
  u.Writeln('                                     �������� ��������� ������');
  u.Writeln(' -maskattach <�����1[;�����2]>       �������� ��� �����, ���������� ���');
  u.Writeln('                                     ��������� ����� ������ (������: *.txt).');
  u.Writeln('                                     ����� ��������� ������ � ������� ���');
  u.Writeln('                                     �������� ��������� ����� � �����!');
  u.writeln(' -type       [text|html]             ��� ��������� (�� ��������� - �����)');
  u.Writeln(' -server     <����� �������>         ����� smtp �������');
  u.Writeln(' -port       <����>                  ���� smtp �������');
  u.Writeln(' -user       [������������]          ��� ������������ ��� �����������');
  u.writeln(' -pass       [������]                ������');
  u.Writeln(' -cpass      [������]                ������������� �������� -crpass ������');
  u.Writeln(' -crpass     <������>                ����������� ��������� ������');
  u.Writeln('');
  u.Writeln(' -imap:save                          �������� ���������� ������������ ����� �');
  u.Writeln('                                     ����� Sent (��� ����� ������) �� �������� �������');
  u.Writeln(' -imap:server <����� �������>        ������ IMAP (���� �� �������, ����� ����������� ��');
  u.Writeln('                                     smtp ������� � ������� smtp �� imap)');
  u.Writeln(' -imap:port <����>                   ���� ������� IMAP (��-��������� = 993)');
  u.Writeln(' -imap:user [������������]           ��� ������������ IMAP (��-��������� �� -user)');
  u.Writeln(' -imap:pass [������]                 ������ IMAP (��-��������� �� -pass ��� -crpass)');
  u.Writeln(' -imap:folder [��� �����]            ���� ��������� ������ (��-��������� = Sent)');
  u.Writeln(' -imap:folders_list                  ������� ������ ����� �� �������� ������� �');
  u.Writeln('                                     ��������� ������ ���������');
  u.Writeln('');
  u.Writeln(' -ssl                                ������������ TLS (��� SMTP � IMAP)');
  u.Writeln(' -proxy      [http|socks4|socks5]    ������������ ������ � ������� ��� ���');
  u.Writeln('                                     �� ��������� ����� ����������� http');
  u.Writeln(' -proxy_addr <[user:pass@]srv:port>  ������ ��� ����������� � ������-�������');
  u.Writeln(' -nosend                             ��������� �������� �������� ������,');
  u.Writeln('                                     �������� ���� ��������� �������');
  u.Writeln(' -nosend_without_files               �� ���������� ������, ���� ��� �������!');
  // 2021.12.20 - ��������� ������� ������� ��������� SMTPSender by Shpirat,
  //              ������� ������� ����������� �������� ���. ������� ������� ������ ������� :)
  u.Writeln(' -xm, -x_mailer <X-Mailer>           �������������� ��������� X-Mailer');
  u.Writeln('                                     ���� ������������ ������ �������� ����������');
  u.Writeln('');
  u.Writeln('');
  u.Writeln(' ** ���������� ������! ��������� ��� ������� � ����������: -h exec **');
  u.Writeln('');
  u.Writeln(' -before_exec <commands>             ��������� �������(�) �� �������� ������');
  u.Writeln(' -before_exec_file <filename>        ��������� ��������� ���� �� �������� ������');
  u.Writeln(' -after_exec <commands>              ��������� �������(�) ����� �������� ������');
  u.Writeln(' -after_exec_file <filename>         ��������� ��������� ���� ����� ��������');
end;

procedure printExecHelp();
begin
  u.Writeln('');
  u.Writeln('������� �� ��������, ����������� �� �/��� ����� �������� ������.');
  u.Writeln('������� ����� ��������� � ���� �������, �������� �� ������������ -- ������� ������ � ������� (;;).');
  u.Writeln('� ����� ������ ������� ����� ����� ��������� � ���� ������� ����� ������� ����� � �������, ���� ������ ������� � ������ ������.');
  u.Writeln('� �������� ����� ������������ ����������, ������� ����� ���������� ��������� ����������, �� ������ ����� ������� ����.');
  u.Writeln('');
  u.Writeln(' �������:');
  u.Writeln('   echo <string>     ������ ������� �� ����� ������');
  u.Writeln('   debug_on          �������� ����� ����������� ������ ����� �� �����������');
  u.Writeln('   debug_off         ��������� ����� ����������� ������');
  u.Writeln('   chdir <dir>       ���������� dir ������� �����������');

  u.Writeln('   show_info  <text> [title]     �������� ����-���������');
  u.Writeln('   show_alert <text> [title]     �������� ��������������');
  u.Writeln('   show_error <text> [title]     �������� ������');

  u.Writeln('   cmd <cmd>         ��������� ��������� ������� cmd � �� ����� � ����������');
  //  u.Writeln('   cmh <cmd>         ������� ��������� ��������� ������� � �� ����� ����������');
  u.Writeln('   exec <cmd>        ��������� ��������� �������, ��������� ����������, �������');
  u.Writeln('                     ��������� ����������');
  //  u.Writeln('   exeh <cmd>        ��������� ��������� ������� �������, ��������� ����������,');
  //  u.Writeln('                     ������� ���������');
  u.Writeln('   pause             ������������� ���������� ��������� �� ������� Enter');
  u.Writeln('   sleep <time>      ������� ����� � time �����������'); //18.05.017
  u.Writeln('   halt              �������� ���������� ���������!');
  u.Writeln('   delete <file>     ������� ��������� ����');
  //added 18.05.017 -- ������ � �����������
  u.Writeln('');
  u.Writeln('   set <param> [val] �������� ���������� � ������ param � ��������� val');
  u.Writeln('   unset <param>     ������� ������������ ���������� � ������ param');

  u.Writeln('');
  u.Writeln('');
  //� ���������� )
  u.Writeln(' ����������. ����� ����� �������� ��� ������������� �������� ');
  u.Writeln('             ���� �������� �� ������ �� ����������.');
  u.Writeln(' ������ ����������: ');
  u.Writeln('');
  u.Writeln('   $program_dir$              ����������, ��� ��������� ���������');
  u.Writeln('   $current_dir$              ������� ����������. ����� ���������� ��');
  u.Writeln('                              ����������, ��� ��������� ��������� ');
  u.Writeln('   $hostname$                 ��� ����� ������ ������');
  u.Writeln('   $username$                 ��� �������� ������������');
  u.Writeln('   $pid$                      PID �������� ��������');
  u.Writeln('');
  u.Writeln('');

  // 18.05.017
  u.Writeln(' �������. ������� -- �� �� �������, ����������� ���������, �� ��� ����� ����� ');
  u.Writeln('          ���������� ���������. �� ����� � ���������� result.');
  u.Writeln(' ������ �������:');
  u.Writeln('');
  u.Writeln('   find_file <dir> <file_mask>     ����� ������� �����, ������������ ');
  u.Writeln('                                   � ����� DIR, ����������� ��� ����� file_mask');
  u.Writeln('   ereg_str <str> <reg_exp> [math_index]   ����� ��������� �� ������ str ��');
  u.Writeln('                                           ����������� ��������� reg_exp,');
  u.Writeln('                                           math_index -- ��� ����� ����������');
  u.Writeln('   file_get_contents <file>        �������� ���������� ����� file, ���� ��');
  u.writeln('                                   ����������');
  u.Writeln('   file_put_contents <file> <text> �������� text � ���� file. ���� ����������,');
  u.writeln('                                   ����� �����������. ��������� -- ����������');
  u.writeln('                                   ���������� � ���� ����');
  //u.Writeln('   http_get_text <URL>             �������� ������� �� URL ������.');

end;

procedure cryptPass();
begin
  if (u.getLastParamValue = '') then
    u.WriteError('�� ������� ������ ��� ����������.')
  else
    u.Writeln('������������� ������: ' + u.cryptPass(u.getLastParamValue()));
end;

procedure setAttachExists();
begin
  // ��������, ��� ������ ���������� (������ ����������).
  sendParams.AddBool('__atach_exists', true);
end;

procedure makeTestParamsFile();
begin
  with sendParams do
  begin
    AddRawLine('#������ ����� � ����������� �������� �����.');
    AddRawLine('#��������! ���������, ���������� ��������� ��������,');
    AddRawLine('#����� ����� ������� ��������� � ����� ������������ ������ ���,');
    AddRawLine('#� �� ��, ��� ���������� � ���� �����.');
    AddRawLine('');
    AddRawLine('#�� ����. ����� ����� � ��� �������');
    AddRawLine('from=');
    AddRawLine('#����. ����� ������� ����� ������� ��������� �����������');
    AddRawLine('to=');
    AddRawLine('#�����. ����� ������� ����� ������� �������������� �����������');
    AddRawLine('cc=');
    AddRawLine('#������� �����. ����� ������� ����� ������� �������������� �����������');
    AddRawLine('bcc=');
    AddRawLine('#���� ���������');
    AddRawLine('subject=');
    AddRawLine('#����� ���������. ��� �������� �� ����� ������ ����������� \n.');
    AddRawLine('body=');
    AddRawLine('#��������� ����� ��������� �� �����. ��� ���� �������� body ����� ��������������');
    AddRawLine('load_body=');
    AddRawLine('#���������� ����. ����� ���������� ��������� ������. ��� ����� ����� ��������� ����� � ����� ���������');
    AddRawLine('attach=');
    AddRawLine('#attach2=');
    AddRawLine('#attach3=');
    AddRawLine('# ����������� ������ �� �����. ��������� ��� �����, ���������� ��� ��������� �����. ��������: *.txt;*.cfg');
    AddRawLine('#maskattach=*.txt;*.cfg;*_small.jpg');
    AddRawLine('#��� ���������. ������� ����� ��� html. �� ��������� ������� �����');
    AddRawLine('type=');
    AddRawLine('#����� SMTP-�������');
    AddRawLine('server=');
    AddRawLine('#���� SMTP-�������. ��������! ��� ������������� TLS ���� ������ ���� ������');
    AddRawLine('port=');
    AddRawLine('#��� ������������ ��� ����������� �� SMTP-�������. ���� ����������� �� �����, ����� �� ���������');
    AddRawLine('user=');
    AddRawLine('#������ ������������. ��� ����� �������� cpass ��������.');
    AddRawLine('pass=');
    AddRawLine('#������������� ������. ��� ����� �� ������� ������ � �������� ����');
    AddRawLine('cpass=');
    AddRawLine('#���������������� ��������� ������, ����� ������������ ���������� ����������.');
    AddRawLine('#ssl');
    AddRawLine('#������������� ������ � ��� ���. ����������������, ����� ������������ ������.');
    AddRawLine('#��-��������� ������������ http, �� �������� ��� ��: socks4, socks5');
    AddRawLine('#proxy');
    AddRawLine('#����� ������ � ������ ��� ����������� �� ���.');
    AddRawLine('#������ �����: server:port ��� user:pass@server:port');
    AddRawLine('proxy_addr=');
    // 05.02.018 -- ���������� �������� ������.
    AddRawLine('# ���������� �������� �������� ������. ����� ��������� ���� ���������� �������.');
    AddRawLine('#nosend=1');
    AddRawLine('# ��������� �������� ������, ���� ��� �� ������ ������.');
    AddRawLine('#nosend_without_files=1');
    //added 03.04.017. ������� �� � ����� �������� �����
    AddRawLine('#������� � ����������! ���������: -h exec');
    AddRawLine('#���������� ������(�) �� �������� ������.');
    AddRawLine('#before_exec=');
    AddRawLine('#���������� ���������� ����� �� �������� ������. ���� ������ ����, �� �������� before_exec ������������.');
    AddRawLine('#before_exec_file=');
    AddRawLine('#���������� ������(�) ����� �������� ������.');
    AddRawLine('#after_exec=');
    AddRawLine('#���������� ���������� ����� ����� �������� ������. ���� ������ ����, �� �������� after_exec ������������.');
    AddRawLine('#after_exec_file=');
    AddRawLine('# ������� ��������� X-Mailer ���� ������������ ������ ����������');
    AddRawLine('#x_mailer=Microsoft Office Outlook, Build 11.0.5510');

    // ��������� 2025.02.25 - ��������� ���������� ����� � ������������ � IMAP
    AddRawLine('');
    AddRawLine('# ��������� ���������� ����� � imap');
    AddRawLine('# ������������ �� ���������� ������������ ����� � ���������?');
    AddRawLine('#imap_save=1');
    AddRawLine('# IMAP ������ (���� �� �������, ����� ������� ������������ �� ��������� server (��������, smtp.yandex.ru ������ imap.yandex.ru)');
    AddRawLine('#imap_server=');
    AddRawLine('# IMAP ����');
    AddRawLine('imap_port=993');
    AddRawLine('# IMAP ������������ (���� �� �������, ����� ���� �� smtp ��������, ������ ���� ���������)');
    AddRawLine('#imap_user=');
    AddRawLine('# IMAP ������ (���� �� �������, ����� ���� �� smtp ��������, ������ ���������)');
    AddRawLine('#imap_pass=');
    AddRawLine('# IMAP ����� ��� ���������� ������ (������ Sent, �� ����� �������� � ��������� ������� ��� ��������� ���������� ������� -imap:list_folders');
    AddRawLine('#imap_folder=Sent');

    AddRawLine(CRLF + '#�����! :) ����������� ����� �������.');
    //
    if (SaveToFile(u.getLastParamValue)) then
      u.WriteInfo('�������� ���� � ����������� ������� ��������.')
    else
      u.WriteError('�� ������� ��������� �������� ���� � �����������...');
    ClearAll;
  end;
end;

procedure LoadParamsFile();
begin
  if (sendParams.LoadFromFile(u.getLastParamValue)) then
    u.WriteInfo('���� ���������� �������� �������!')
  else //���� ������ ��� ��������
    u.WriteError('�� ������� ��������� ���� ���������� "' + u.getLastParamValue
      + '"');
end;

procedure fillAllAttaches();
//�������� ��� ��������� ������
var
  i: integer;
  attach_count: Integer;
begin
  attach_count := 1;
  for i := 1 to u.pcount do
  begin
    if ((u.p(i) = 'attach') or (u.p(i) = '-attach') or (u.p(i) = '--attach'))
      then
    begin
      sendParams.AddStr('attach_' + IntToStr(attach_count),
        u.getLastParamValue);
      Inc(attach_count);
    end;
  end;
end;

// added 05.02.018 -- ����������� ��� ����� �� ����� ��� ������

procedure maskAttachFiles(masks: string);
var
  mask_list: TStringList;
  i: Integer;
  current_mask, dir, fn: string;
  srec: TSearchRec;
begin
  mask_list := TStringList.create;
  // ���� ����� ��� ������� ��������� �����, ���� �� ��� �������� )
  mask_list.Delimiter := ';';
  mask_list.DelimitedText := masks;

  // � ������ ���� �������� �� ���� ������ � �������� ��� ����� �� ���! )
  for i := 0 to mask_list.Count - 1 do
  begin
    current_mask := trim(mask_list.Strings[i]);
    dir := ExtractFileDir(current_mask);
    if (dir <> '') then
      dir := dir + '\';

    if (FindFirst(current_mask, faAnyFile, srec) = 0) then
      repeat
        fn := dir + srec.Name;
        setAttachExists();
        if smtp.AddAttachFile(fn) then
        begin
          u.WriteInfo('�������� ���� �� �����: "' + fn + '"');
        end;
      until FindNext(srec) <> 0;

    FindClose(srec);
  end; // .for
  mask_list.Free;
end;

function checkRequestedParams(): Boolean; //�������� ������������ ����������.
begin
  if (sendParams.isEmptyParam('from')) then
    u.WriteError('�������� -from �������� ������������');
  if (sendParams.isEmptyParam('to')) then
    u.WriteError('�������� -to �������� ������������');

  {
  // 23.05.017 -- ��������� "����" � "����� ������" �� ����������� ������ )
  if (sendParams.isEmptyParam('subject')) then
    u.WriteError('�������� -subject �������� ������������');
  if (sendParams.isEmptyParam('body') and (sendParams.isEmptyParam('load_body')))
    then
    u.WriteError('�� ����� ����� ���������. ����� ������� �������� -body ��� load_body');
  }
  if (sendParams.isEmptyParam('server')) then
    u.WriteError('�������� -server �������� ������������');

  if (sendParams.GetBool('imap_save')) then
  begin
    // Nothing? Using defaults :)
  end;

  result := not u.ErrorExists;
end;

{*********************************************************}

procedure addAllAttaches();
var
  i: Integer;
  s: string;
  //
  filepath, filename: string;
begin
  for i := 0 to sendParams.count - 1 do
  begin
    s := sendParams.GetParamNameOfIndex(i);

    // �������� ������� �� �����!
    if (u.compareWithLenght(s, 'maskattach') and (not
      sendParams.GetBool('__mask_attach_added', false))) then
    begin
      maskAttachFiles(sendParams.GetValueOfIndex(i));
      Continue;
    end;

    if (u.compareWithLenght(s, 'attach')) then
    begin
      s := sendParams.GetStr(s);
      if FileExists(s) then
        setAttachExists();
      if (Pos(';', s) <> 0) then
      begin
        filepath := SeparateLeft(s, ';');
        filename := SeparateRight(s, ';');
        if smtp.AddAttachFile(filepath, filename) then
          u.WriteInfo('���� "' + filepath + '" � ������ "' + filename +
            '" ������� ��������!')
        else
          u.WriteError('�� ������� ���������� ���� "' + filepath +
            '": ���� �� ������!');
      end
      else
      begin
        if smtp.AddAttachFile(s) then
          u.WriteInfo('���� "' + s + '" ������� ��������!')
        else
          u.WriteError('�� ������� ���������� ���� "' + s +
            '": ���� �� ������!');
      end;
    end;
  end;
end;

procedure setIMAPParams();
begin
  with smtp do
  begin
    IMAPSave := sendParams.GetBool('imap_save');

    IMAPHost := sendParams.GetStr('imap_server', StringReplace(SMTPHost, 'smtp',
      'imap', [rfIgnoreCase]));

    IMAPPort := sendParams.GetStr('imap_port', '993');
    IMAPUser := sendParams.GetStr('imap_user', SMTPUser);
    IMAPPass := sendParams.GetStr('imap_pass', SMTPPass);
    IMAPFolder := sendParams.GetStr('imap_folder', 'Sent');
  end;
end;

procedure setSMTPParams();
var
  pr: TProxyParams;
begin
  addAllAttaches();
  with smtp do
  begin
    SMTPHost := sendParams.GetStr('server');
    SMTPPort := sendParams.GetStr('port');
    SMTPUser := sendParams.GetStr('user');
    SMTPPass := sendParams.GetStr('pass');
    if (SMTPPass = '') then
      if (sendParams.GetStr('cpass') <> '') then
        SMTPPass := u.decryptPass(sendParams.GetStr('cpass'));
    if (sendParams.ParamExists('ssl')) then
      UseSSL := true;

    SendFrom := sendParams.GetStr('from');
    SendTo := sendParams.GetStr('to');
    SendCC := sendParams.GetStr('cc');
    SendBCC := sendParams.GetStr('bcc');
    Subject := sendParams.GetStr('subject');
    Body := sendParams.GetStr('body',
      u.getTextFromFile(sendParams.GetStr('load_body')));
    Body := AnsiReplaceStr(Body, '\n', CRLF);
    MessageType := getMessageTypeFromStr(sendParams.GetStr('type'));
    XMailer := sendParams.GetStr('x_mailer', XMailer);

    if (sendParams.ParamExists('proxy')) then
    begin
      UseProxy := True;
      ProxyType := getProxyTypeFromStr(sendParams.GetStr('proxy'));
      pr := getProxyParamsFromStr(sendParams.GetStr('proxy_addr'));
      ProxyHost := pr.Host;
      ProxyPort := pr.Port;
      ProxyUser := pr.User;
      ProxyPass := pr.Pass;
    end;

    setIMAPParams();
  end;
end;

procedure sendEmail(FakeSend: Boolean = false);
begin
  smtp.SendMessage(FakeSend);
end;

{
  ��������� 03.04.017
  ���������� ��������� ������ �� � ����� ������� ������
  �������������� ����� ����������.
  $current_dir$   -- ������� ����������
  $hostname$      -- ��� �����

}

procedure BeforeExec();
begin
  if (sendParams.ParamExists('before_exec_file')) then
  begin
    with TCommandListParser.Create('', varsManager) do
    begin
      LoadFromFile(sendParams.GetStr('before_exec_file'));
      ExecList();
      Free;
    end;
    exit;
  end;

  if (sendParams.ParamExists('before_exec')) then
  begin
    with TCommandListParser.Create('', varsManager) do
    begin
      ParseCommandList(sendParams.GetStr('before_exec'));
      ExecList();
      Free;
    end;
  end;
end;

procedure AfterExec();
begin
  if (sendParams.ParamExists('after_exec_file')) then
  begin
    with TCommandListParser.Create('', varsManager) do
    begin
      LoadFromFile(sendParams.GetStr('after_exec_file'));
      ExecList();
      Free;
    end;
    exit;
  end;

  if (sendParams.ParamExists('after_exec')) then
  begin
    with TCommandListParser.Create('', varsManager) do
    begin
      ParseCommandList(sendParams.GetStr('after_exec'));
      ExecList();
      Free;
    end;
  end;
end;

{
  //./
}

procedure parseParams;
var
  help_showed: Boolean; //���� �� �������� ������� �� ������������ �������?
  help_command: string;
begin
  u.Writeln('smtpsender v' + version);

  //����� ������� �� ��������.
  if (u.paramExists(['-h', '-help'])) then
  begin
    //���������� �����!
    help_showed := false;
    //�������� ������ ������� (��������: -h exec)
    help_command := u.getLastParamValue;

    //���� ������ ������� ������� �� ���� ��������.
    if (help_command = '') then
    begin
      printHelp();
      help_showed := True;
    end;

    if (help_command = 'exec') then
    begin
      printExecHelp();
      help_showed := true;
    end;

    //��� ����� ���� ��� �������...

    //���� ������ ������� ��� ������, �� ���� �������� �� ����
    if not help_showed then
      u.WriteError('�� ������ ������ ������� � ������ "' + help_command + '"');
    exit; //������� ��������, ����� �����.
  end;

  //������� ��� �������� �������������� ������
  if (u.paramExists('-crpass')) then
  begin
    cryptPass();
    exit;
  end;

  if (u.paramExists('-make_data')) then
  begin
    makeTestParamsFile();
    exit;
  end;

  //�������� ���������� �������� ����� �� �����
  if (u.paramExists('-load_data')) then
  begin
    loadParamsFile();
  end
  else
  begin
    //04.04.017! ���� ���� �� ������ ���� � �����������, �� ����� �����������
    //�������� ���� � ������ smtpsend.conf � ����� � ���������� � ��������� ���!
    if (FileExists(u.GetApplicationDir + 'smtpsend.conf')) then
    begin
      sendParams.LoadFromFile(u.GetApplicationDir + 'smtpsend.conf');
    end;
  end;

  if (u.paramExists('-imap:save')) then
    sendParams.AddBool('imap_save', true);
  if (u.paramExists('-imap:server')) then
    sendParams.AddStr('imap_server', u.getLastParamValue);
  if (u.paramExists('-imap:port')) then
    sendParams.AddStr('imap_port', u.getLastParamValue);
  if (u.paramExists('-imap:user')) then
    sendParams.AddStr('imap_user', u.getLastParamValue);
  if (u.paramExists('-imap:pass')) then
    sendParams.AddStr('imap_pass', u.getLastParamValue);
  if (u.paramExists('-imap:port')) then
    sendParams.AddStr('imap_port', u.getLastParamValue);
  if (u.paramExists('-imap:folder')) then
    sendParams.AddStr('imap_folder', u.getLastParamValue);

  if (u.paramExists('-from')) then
    sendParams.AddStr('from', u.getLastParamValue);
  if (u.paramExists('-to')) then
    sendParams.AddStr('to', u.getLastParamValue);
  if (u.paramExists('-cc')) then
    sendParams.addStr('cc', u.getLastParamValue);
  if (u.paramExists('-bcc')) then
    sendParams.addStr('bcc', u.getLastParamValue);
  if (u.paramExists('-subject')) then
    sendParams.AddStr('subject', u.getLastParamValue);
  if (u.paramExists('-body')) then
    sendParams.AddStr('body', u.getLastParamValue);
  if (u.paramExists('-load_body')) then
    sendParams.AddStr('load_body', u.getLastParamValue);
  if (u.paramExists('-type')) then
    sendParams.AddStr('type', u.getLastParamValue);
  if (u.paramExists('-server')) then
    sendParams.AddStr('server', u.getLastParamValue);
  if (u.paramExists('-port')) then
    sendParams.AddStr('port', u.getLastParamValue);
  if (u.paramExists('-user')) then
    sendParams.AddStr('user', u.getLastParamValue);
  if (u.paramExists('-pass')) then
    sendParams.AddStr('pass', u.getLastParamValue);
  if (u.paramExists('-cpass')) then
    sendParams.AddStr('cpass', u.getLastParamValue);
  if (u.paramExists('-ssl')) then
    sendParams.AddBool('ssl', true);
  if (u.paramExists('-proxy')) then
    sendParams.AddStr('proxy', u.getLastParamValue);
  // 2021.12.20 - X-Mailer ������ ����� ������, ��
  if (u.paramExists('-x_mailer') or u.paramExists('-xm')) then
    sendParams.AddStr('x_mailer', u.getLastParamValue);
  // 15.02.019 -- ������� �������� proxyaddr, � �� ����� ����� ������������� ������ ������ � ����!
  if (u.paramExists('-proxy_addr')) or (u.paramExists('-proxyaddr')) then
    sendParams.AddStr('proxy_addr', u.getLastParamValue);
  if u.paramExists('-attach') then
    fillAllAttaches; //���� ����� ���� ��������� �������. ���� ��� ���������!
  // added 05.02.018 -- ����������� ������ �� �����!
  if u.paramExists('-maskattach') then
  begin
    maskAttachFiles(u.getLastParamValue);
    // ��� ����� ��� ����, ����� ����� ��������� ������ ���� �������� �������
    // ��� ��������������, ���� �� ���� � ����� ��������.
    sendParams.AddBool('__mask_attach_added', true);
  end;

  //03.04.017 ������� �� � ����� �������� )
  if (u.paramExists('-before_exec')) then
    sendParams.AddStr('before_exec', u.getLastParamValue);
  if (u.paramExists('-before_exec_file')) then
    sendParams.AddStr('before_exec_file', u.getLastParamValue);
  if (u.paramExists('-after_exec')) then
    sendParams.AddStr('after_exec', u.getLastParamValue);
  if (u.paramExists('-after_exec_file')) then
    sendParams.AddStr('after_exec_file', u.getLastParamValue);

  // 05.02.018 -- ���� true, �� �������� �� ����� �������������!
  if (u.paramExists('-nosend')) then
    sendParams.AddBool('nosend', true);
  if (u.paramExists('-nosend_without_files')) then
    sendParams.AddBool('nosend_without_files', true);

  // 2025.02.25 -- ��������� ���������� ������������ ��������� � ��������� (IMAP)

  if (checkRequestedParams()) then
  begin
    // Added 18.05.017 -- ���������� ���������� � ������ ����������. �����-��.
    varsManager := TVarReplacer.Create(sendParams.getStringsObject);

    // �������� � ���������� ������, ��� ���� �� ��������
    BeforeExec();

    // � ������ ��������, ���������� � ���������, ����� ����� ���� ���������� � beforeExec ���������� � �������������� ��������� ������������
    sendParams.SetAllText(varsManager.GetAllText);

    // � BeforeExec �� ����� ���-�� �������� ��� ������� ���������, ������� ���� �� ��������� �����!
    if (not checkRequestedParams()) then
    begin
      u.WriteInfo('��������� ���������� ���� ����������� � ���� ���������� ������ � before_exec');
      Readln;
      exit;
    end;

    // ��������� SMTP �������, �������� ������, �������...
    setSMTPParams();

    // 2025.02.25 - ��������� ������ ����� � IMAP �������
    if ({sendParams.GetBool('imap_save') and}u.paramExists('-imap:folders_list'))
      then
    begin
      smtp.printImapFoldersList();
      halt(0);
    end;

    // �����, ���� ����������� ��������� �����, � �� ���!
    if (sendParams.GetBool('nosend_without_files', false) and not
      sendParams.GetBool('__atach_exists', false)) then
    begin
      u.WriteError('������ �� ����������, ��� ��� �� ����������� �� ������ ������, � � ���������� �������, ��� ��� ����������� (�������� nosend_without_files)');
      exit;
    end;

    //��������������� ������� ������... )
    sendEmail(sendParams.GetBool('nosend', false));

    // �������� � ���������� ������, ��� ���� ����� ��������
    AfterExec();
    exit; //���� ��������� ��� �� �����, �� ����� �����.
  end;

  //���� �� ����� ����� ����� �� ��� ���������, ������ �� ���� �������� �� �������
  //������� ����� �������� � ���, ��� ������������ ���������� ��� :)
  u.WriteError('�� ������� ������������ ���������.');
  u.writeInfo('����������� �������� -h ��� ������� �� ��������.');
  Halt(1);
end;

{ TSMTPErrorGetter }

constructor TSMTPErrorGetter.Create;
begin
  smtp.OnSocketError := OnSocketError;
  smtp.OnSMTPError := onSMTPError;
  smtp.OnSMTPInfo := onSMTPInfo;
end;

procedure TSMTPErrorGetter.onSMTPError(err: string; from: string = 'smtp');
begin
  from := AnsiUpperCase(from);
  u.WriteError(from + ': ' + err);
end;

procedure TSMTPErrorGetter.onSMTPInfo(info: string; from: string = 'smtp');
begin
  from := AnsiUpperCase(from);
  u.WriteInfo(from + ': ' + info);
end;

procedure TSMTPErrorGetter.OnSocketError(ErrCode: integer;
  ErrDescription: string);
begin
  u.WriteError('socket error(' + IntToStr(ErrCode) + '): ' + ErrDescription);
end;

{***************************************************************************}

begin
  u := TUtils.Create;
  sendParams := TParamParser.Create();
  //varsManager := TVarReplacer.Create(); //������������� ����� ����������� ������!
  smtp := TSMTPSender.create;
  TSMTPErrorGetter.Create; //�� ����� ��� ����� ������ �� SMTPSender-�
  //������ ���������, ��������� ����������� ������ ����������
  parseParams();
  if (u.ErrorExists) then
    Halt(1)
  else
    Halt(0);
end.

